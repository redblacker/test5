import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';

import Home from './views/Home';
import Contact from './views/Contact';
import About from './views/About';


import VueRouter from 'vue-router';

Vue.use(VueRouter);

// import './scss/main.scss';



Vue.config.productionTip = false

const router = new VueRouter({
  routes : [
    {path :'/',component :Home},
    {path :'/Contact',component :Contact},
    {path :'/About',component :About},
  ]
})



new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
